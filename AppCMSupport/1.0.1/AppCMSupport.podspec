Pod::Spec.new do |s|
  s.name             = "AppCMSupport"
  s.version          = "1.0.1"
  s.summary          = "A short description of AppCMSupport."
  s.description      = <<-DESC
                       An optional longer description of AppCMSupport
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/AppCMSupport"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { 
    :git => "git@bitbucket.org:plusr/AppCMSupport.git",
    :tag => s.version.to_s 
  }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'AppCMSupport' => ['Pod/Assets/*.xib']
  }

  s.frameworks = 'UIKit'
  s.dependency 'AppCM'
  s.dependency 'AppDavis-iOS-SDK'
  s.dependency 'PromiseKit'
end
