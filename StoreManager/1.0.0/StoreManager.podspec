Pod::Spec.new do |s|
  s.name     = 'StoreManager'
  s.version  = '1.0.0'
  s.license  = { :type => 'MIT', :file => 'LICENSE' }
  s.summary  = 'Hiroba Library'
  s.homepage = 'https://bitbucket.org/plusr/StoreManager'
  s.ios.deployment_target = '8.0'
  s.license  = { :type => 'MIT', :file => 'LICENSE' }
  s.author   = { 'Akuraru IP' => 'akuraru@gmail.com' }
  s.source   = {
    :git => "git@bitbucket.org:plusr/StoreManager.git",
    :tag => s.version.to_s
  }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'StoreManager' => ['Pod/Assets/**/*']
  }
  s.frameworks = 'UIKit', 'StoreKit'
  s.dependency 'RMStore/AppReceiptVerificator'
  s.dependency 'RMStore/KeychainPersistence'
end
