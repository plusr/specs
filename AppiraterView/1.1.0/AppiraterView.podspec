Pod::Spec.new do |s|
  s.name             = "AppiraterView"
  s.version          = "1.1.0"
  s.summary          = "AppiraterでUIAlertViewの代わりにViewControllerを渡して表示する"
  s.description      = <<-DESC
                       このライブラリは社内WebView用のライブラリです
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/AppiraterView"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = {
    :git => "git@bitbucket.org:plusr/AppiraterView.git", :tag => s.version.to_s }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  
  s.frameworks = 'UIKit'
  s.dependency 'Masonry'
end
