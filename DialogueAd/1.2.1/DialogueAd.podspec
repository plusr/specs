#
# Be sure to run `pod lib lint DialogueAd.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "DialogueAd"
  s.version          = "1.2.1"
  s.summary          = "A short description of DialogueAd."
  s.description      = <<-DESC
                       An optional longer description of DialogueAd

                       * Markdown format.
                       * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/DialogueAd"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:plusr/DialogueAd.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'DialogueAd' => ['Pod/Assets/*.xib']
  }

  s.frameworks = 'UIKit'
  s.dependency 'Lambda-Alert'
  s.dependency 'AZEncodeURIComponent'
  s.dependency 'AMoAd'
  s.dependency 'Masonry'
end
