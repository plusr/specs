#
# Be sure to run `pod lib lint KaradanoteWebViewController.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "KNPBundle"
  s.version          = "0.1.0"
  s.summary          = "reader info.plist "
  s.description      = <<-DESC
                       このライブラリは社内WebView用のライブラリです
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/KNPBundle"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:plusr/KNPBundle.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/akuraru'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/'
  s.resource_bundles = {
    'KNPBundle' => ['Pod/Assets/*.png']
  }
  
  s.frameworks = 'Foundation'
end
