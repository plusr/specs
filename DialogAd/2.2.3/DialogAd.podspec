#
# Be sure to run `pod lib lint DialogAd.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "DialogAd"
  s.version          = "2.2.3"
  s.summary          = "A short description of DialogAd."
  s.description      = <<-DESC
                       An optional longer description of DialogAd
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/DialogueAd"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:plusr/DialogueAd.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/akuraru'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'DialogAd' => ['Pod/Assets/*.xib']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  s.dependency 'Lambda-Alert'
  s.dependency 'AZEncodeURIComponent'
  s.dependency 'AMoAd'
  s.dependency 'Masonry'
  s.dependency 'LupinusHTTP'
  s.dependency 'AppDavis-iOS-SDK'
end
