Pod::Spec.new do |s|
  s.name             = "DialogAd"
  s.version          = "2.4.6"
  s.summary          = "A short description of DialogAd."
  s.description      = <<-DESC
                       An optional longer description of DialogAd
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/DialogueAd"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = {
    :git => "git@bitbucket.org:plusr/DialogueAd.git",
    :tag => s.version.to_s
  }
  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'DialogAd' => ['Pod/Assets/*.xib']
  }

  s.frameworks = 'UIKit'
  s.dependency 'Lambda-Alert'
  s.dependency 'AZEncodeURIComponent'
  s.dependency 'Masonry'
  s.dependency 'LupinusHTTP'
  s.dependency 'GoogleAnalytics'
end
