Pod::Spec.new do |s|
  s.name     = 'HirobaLibrary'
  s.version  = '1.0.2'
  s.license  = { :type => 'MIT', :file => 'LICENSE' }
  s.summary  = 'Hiroba Library'
  s.homepage = 'https://bitbucket.org/plusr/HirobaLibrary'
  s.ios.deployment_target = '7.0'
  s.license  = { :type => 'MIT', :file => 'LICENSE' }
  s.author   = { 'Akuraru IP' => 'akuraru@gmail.com' }
  s.source   = {
    :git => "git@bitbucket.org:plusr/HirobaLibrary.git",
    :tag => s.version.to_s
  }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'HirobaLibrary' => ['Pod/Assets/**/*']
  }

  s.frameworks = 'UIKit'
  s.dependency 'Lambda-Alert'
  s.dependency 'PromiseKit', '1.5.3'
  s.dependency 'OMGHTTPURLRQ', '2.1.3'
  s.dependency 'Asterism'
  s.dependency 'ArsDashFunction'
  s.dependency 'HMSegmentedControl', '~> 1.3.0'
  s.dependency 'RoleTabBarController'
  s.dependency 'ActionSheetPicker-3.0', '~> 1.1.0'
  s.dependency 'UIImage-Teeny'
  s.dependency 'KVOController'
  s.dependency 'Lambda-Alert'
  s.dependency 'UITextSubClass'
  s.dependency 'UITextFieldWithLimit'
  s.dependency 'CrayWebViewController'
  s.dependency 'SimpleUserDefaults'
  s.dependency 'PHFComposeBarView', '~> 2.0.1'
  s.dependency 'JASidePanels'
  s.dependency "ErrorGen"
  s.dependency 'NSDictionaryAsURLQuery'
  s.dependency 'Mantle', '~> 1.5'
  s.dependency "LupinusHTTP"
  s.dependency 'NSDate+TimeAgo', '~> 1.0.3'
  s.dependency "UIAlertView-NSErrorAddition"
  s.dependency "isUnitTesting"
  s.dependency "NSDate-Escort"
  s.dependency 'CSNLINEOpener'
  s.dependency 'CrayWebViewController'
  s.dependency "RequestInMemory"
  s.dependency 'Appirater'
  s.dependency 'AppVersioning'
  s.dependency 'AMoAd'
  s.dependency 'AAMFeedback'
  s.dependency 'GAIService/Core'
  s.dependency 'Realm', '0.94.1'
  s.dependency 'KaradanoteWebViewController'
end
