#
# Be sure to run `pod lib lint BirthdateAdvertisement.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "BirthdateAdvertisement"
  s.version          = "0.1.0"
  s.summary          = "生年月日通知広告"
  s.description      = <<-DESC
                       生年月日から一定以上経過した場合広告として通知を行う。
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/BirthdateAdvertisement"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:plusr/BirthdateAdvertisement.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/akuraru'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'BirthdateAdvertisement' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'NSDate-Escort'
  s.dependency 'AZDateBuilder'
  s.dependency 'Lambda-Alert'
end
