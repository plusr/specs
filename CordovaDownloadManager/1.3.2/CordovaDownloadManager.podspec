Pod::Spec.new do |s|
  s.name             = "CordovaDownloadManager"
  s.version          = "1.3.2"
  s.summary          = "This library will be used in order to connect or cut NSDate."
  s.description      = <<-DESC
                        + (NSDate *)day:(NSDate *)date nextWeekday:(NSInteger)weekday;
                       DESC
  s.homepage         = 'https://bitbucket.org/plusr/CordovaDownloadManager'
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:plusr/cordovadownloadmanager.git", :tag => s.version.to_s }

  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.source_files = 'Pod/Classes'
  
  s.frameworks = 'Foundation'
  s.dependency 'SSZipArchive'
end
