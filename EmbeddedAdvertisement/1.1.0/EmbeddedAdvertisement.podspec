Pod::Spec.new do |s|
  s.name             = "EmbeddedAdvertisement"
  s.version          = "1.1.0"
  s.summary          = "A short description of EmbeddedAdvertisement."
  s.description      = <<-DESC
                       An optional longer description of EmbeddedAdvertisement
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/DialogueAd"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = {
    :git => "git@bitbucket.org:plusr/embeddedadvertisement.git",
    :tag => s.version.to_s 
  }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'EmbeddedAdvertisement' => ['Pod/Assets/*.png']
  }
  
  s.frameworks = 'UIKit'
  s.dependency 'AZEncodeURIComponent'
  s.dependency 'Masonry'
  s.dependency 'LupinusHTTP'
end
