
Pod::Spec.new do |s|
  s.name             = "KaradanoteWebViewController"
  s.version          = "1.5.5"
  s.summary          = "webView lib"
  s.description      = <<-DESC
                       このライブラリは社内WebView用のライブラリです
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/karadanotewebviewcontroller"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:plusr/karadanotewebviewcontroller.git", :tag => s.version.to_s }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'KaradanoteWebViewController' => ['Pod/Assets/*.png']
  }
  
  s.frameworks = 'UIKit'
  s.dependency 'CrayWebViewController/Core'
  s.dependency 'CrayWebViewController/CrayNavigationController'
  s.dependency 'WebViewJavascriptBridge'
  s.dependency 'iOSInstalledApps'
  s.dependency 'Masonry'
  s.dependency 'TUSafariActivity'
  s.dependency 'GAIService/Core'
end
