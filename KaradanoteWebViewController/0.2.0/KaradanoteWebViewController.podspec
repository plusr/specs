#
# Be sure to run `pod lib lint KaradanoteWebViewController.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "KaradanoteWebViewController"
  s.version          = "0.2.0"
  s.summary          = "webView lib"
  s.description      = <<-DESC
                       このライブラリは社内WebView用のライブラリです
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/karadanotewebviewcontroller"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:plusr/karadanotewebviewcontroller.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/akuraru'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'KaradanoteWebViewController' => ['Pod/Assets/*.png']
  }
  
  s.frameworks = 'UIKit'
  s.dependency 'CrayWebViewController'
  s.dependency 'WebViewJavascriptBridge'
  s.dependency 'iOSInstalledApps'
  s.dependency 'Masonry'
  s.dependency 'Lambda-Alert'
end
