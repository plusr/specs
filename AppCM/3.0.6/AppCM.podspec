Pod::Spec.new do |s|
  s.name     = 'AppCM'
  s.version  = '3.0.6'
  s.license  = { :type => 'MIT', :file => 'LICENSE' }
  s.summary  = 'AppCM Library'
  s.homepage = 'https://bitbucket.org/plusr/AppCM'
  s.ios.deployment_target = '7.0'
  s.license  = { :type => 'MIT', :file => 'LICENSE' }
  s.author   = { 'Akuraru IP' => 'akuraru@gmail.com' }
  s.source   = {
    :git => "git@bitbucket.org:plusr/AppCM.git",
    :tag => s.version.to_s
  }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resources = 'Pod/Assets/**/*'
  s.vendored_library = 'Pod/libAppCM.a'

  s.frameworks = 'AdSupport'
  s.frameworks = 'AVFoundation'
  s.frameworks = 'CoreFoundation'
  s.frameworks = 'CoreGraphics'
  s.frameworks = 'CoreMedia'
  s.frameworks = 'CoreTelephony'
  s.frameworks = 'Foundation'
  s.frameworks = 'MediaPlayer'
  s.frameworks = 'Security'
  s.frameworks = 'UIKit'
  s.library = 'sqlite3'
end
