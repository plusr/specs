#
# Be sure to run `pod lib lint GAIService.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "GAIService"
  s.version          = "0.1.3"
  s.summary          = "GAI wrapper"
  s.description      = <<-DESC
                       Google Analtics wrapper 
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/GAIService"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:plusr/GAIService.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'GAIService' => ['Pod/Assets/*.png']
  }
  s.dependency 'GoogleAnalytics-iOS-SDK'
  s.dependency 'ApplicationChecker'
  s.dependency 'NSDate-Escort'
end
