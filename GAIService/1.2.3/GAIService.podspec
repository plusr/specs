Pod::Spec.new do |s|
  s.name             = "GAIService"
  s.version          = "1.2.3"
  s.summary          = "GAI wrapper"
  s.description      = <<-DESC
                       Google Analtics wrapper
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/GAIService"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = {
    :git => "git@bitbucket.org:plusr/GAIService.git",
    :tag => s.version.to_s
  }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.subspec 'Core' do |a|
    a.source_files = 'Pod/Classes'
    a.resource_bundles = {
      'GAIService' => ['Pod/Assets/*.png']
    }
    a.dependency 'GoogleAnalytics'
    a.dependency 'ApplicationChecker'
    a.dependency 'NSDate-Escort'
  end

  s.subspec 'GoogleIDFASupport' do |a|
    a.dependency 'GAIService/Core'
    a.dependency 'GoogleIDFASupport'
  end
end
