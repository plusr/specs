Pod::Spec.new do |s|
  s.name             = "cordova-plugin-plusr"
  s.version          = "1.2.1"
  s.summary          = "cordova-plugin-plusr"
  s.description      = <<-DESC
                       Plusr用のCordovaプラグイン

                       閉じたりブラウザ開いたりする
                       DESC
  s.homepage = 'https://bitbucket.org/plusr/cordova-plugin-plusr'
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = {
    :git => "https://plusr_android:RY3gLNsnneY4rLNn@bitbucket.org/plusr/cordova-plugin-plusr.git",
    :tag => s.version.to_s
  }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'src/ios/KNPThemeableBrowser.{h,m}', 'src/ios/PlusrPlugin.{h,m}'
  s.frameworks = 'UIKit'
end
