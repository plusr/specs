
Pod::Spec.new do |s|
  s.name             = "ApplicationChecker"
  s.version          = "0.1.11"
  s.summary          = "アプリ間遷移用ライブラリ"
  s.description      = <<-DESC
                       アプリ間遷移用ライブラリ自社アプリと競合アプリの情報取得も兼ねている
                       DESC
  s.homepage         = "https://bitbucket.org/plusr/ApplicationChecker"
  s.license          = 'MIT'
  s.author           = { "akuraru" => "akuraru@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:plusr/ApplicationChecker.git", :tag => s.version.to_s }

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.frameworks = 'UIKit'
end
